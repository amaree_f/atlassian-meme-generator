package obr;

import com.atlassian.plugin.webresource.async.obr.WebResourceLoaderObrResolver;
import com.atlassian.prettyurls.obr.PrettyUrlsObrResolver;

/**
 * Hack to call OBR'd plugins, as a java reference is needed for them to be resolved
 * @since v6.0
 */
public class ObrResolution
{
    static
    {
        PrettyUrlsObrResolver.resolved();
        WebResourceLoaderObrResolver.resolved();
    }
}
