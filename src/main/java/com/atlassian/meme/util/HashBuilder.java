package com.atlassian.meme.util;

import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Utility for building content hash
 * @since v1.0
 */
public class HashBuilder
{
    private static final String UTF8 = "UTF-8";
    private static final String MD5 = "MD5";

    public static String buildHash(String... values)
    {
        try
        {
            MessageDigest md5 = MessageDigest.getInstance(MD5);
            for (String value : values)
            {
                md5.update(value.getBytes(UTF8));
            }
            return new String(Hex.encodeHex(md5.digest()));
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException("MD5 hashing algorithm is not available.", e);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new AssertionError("UTF-8 encoding is not available.");
        }
    }
}
