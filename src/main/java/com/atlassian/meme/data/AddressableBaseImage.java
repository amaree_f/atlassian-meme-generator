package com.atlassian.meme.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Base image with a url
 * @since v1.0
 */
@XmlRootElement
public class AddressableBaseImage
{
    @XmlElement
    private String key;

    @XmlElement
    private String name;

    @XmlElement
    private String url;

    public AddressableBaseImage(String key, String name, String url)
    {
        this.key = key;
        this.name = name;
        this.url = url;
    }

    public String getKey()
    {
        return key;
    }

    public String getName()
    {
        return name;
    }

    public String getUrl()
    {
        return url;
    }
}
