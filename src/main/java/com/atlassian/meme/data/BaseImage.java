package com.atlassian.meme.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Base images for memes
 * @since v1.0
 */
@XmlRootElement
public class BaseImage
{
    @XmlElement
    private String contentType;

    @XmlElement
    private String base64Data;

    @XmlElement
    private String key;

    @XmlElement
    private String name;

    private BaseImage()
    {
    }

    public BaseImage(String contentType, String base64Data, String key, String name)
    {
        this.contentType = contentType;
        this.base64Data = base64Data;
        this.key = key;
        this.name = name;
    }

    public String getContentType()
    {
        return contentType;
    }

    public String getBase64Data()
    {
        return base64Data;
    }

    public String getKey()
    {
        return key;
    }

    public String getName()
    {
        return name;
    }
}
