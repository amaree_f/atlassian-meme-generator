package com.atlassian.meme.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Meme image
 * @since v1.0
 */
@XmlRootElement
public class Meme
{
    @XmlElement
    private String key;

    @XmlElement
    private String name;

    @XmlElement
    private String contentType;

    @XmlElement
    private String base64Data;

    private Meme()
    {
    }

    public Meme(String key, String name, String contentType, String base64Data)
    {
        this.key = key;
        this.name = name;
        this.contentType = contentType;
        this.base64Data = base64Data;
    }

    public String getKey()
    {
        return key;
    }

    public String getName()
    {
        return name;
    }

    public String getContentType()
    {
        return contentType;
    }

    public String getBase64Data()
    {
        return base64Data;
    }
}
