package com.atlassian.meme.servlet;

import com.atlassian.meme.service.AddressableService;
import com.atlassian.meme.service.MemeService;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

// TODO: unused, see atlassian-plugin.xml
public class MemeServlet extends HttpServlet
{
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final MemeService memeService;
    private final AddressableService addressableService;

    public MemeServlet(SoyTemplateRenderer soyTemplateRenderer, MemeService memeService, AddressableService addressableService)
    {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.memeService = memeService;
        this.addressableService = addressableService;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        render(request, response, "meme.page.gallery", ImmutableMap.<String, Object>of(
                "memes", addressableService.memes(request.getContextPath(), memeService.getMemes())
        ));
    }

    private void render(HttpServletRequest request, HttpServletResponse response, String template, Map<String, Object> data)
            throws ServletException, IOException
    {
        String moduleKey = "com.atlassian.meme.atlassian-meme-generator:server-templates";
        try
        {
            soyTemplateRenderer.render(response.getWriter(), moduleKey, template, data);
            response.getWriter().close();
            response.setStatus(HttpServletResponse.SC_OK);
        }
        catch (SoyException ex)
        {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            ex.printStackTrace(response.getWriter());
        }
    }
}
