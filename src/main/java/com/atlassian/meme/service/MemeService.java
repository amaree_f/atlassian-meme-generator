package com.atlassian.meme.service;

import com.atlassian.meme.data.BaseImage;
import com.atlassian.meme.data.Meme;

import java.util.List;

/**
 * Service for creating and retrieving memes and base images
 * @since v6.0
 */
public interface MemeService
{
    public BaseImage createBaseImage(String contentType, String data);

    public BaseImage createBaseImage(BaseImage baseImage);

    public void persist(Meme meme);

    public List<Meme> getMemes();

    public Meme getMeme(String key);

    public List<BaseImage> getBaseImages();

    public BaseImage getBaseImage(String key);

    public void clearAll();
}
