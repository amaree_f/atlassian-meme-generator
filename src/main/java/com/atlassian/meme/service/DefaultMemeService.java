package com.atlassian.meme.service;

import com.atlassian.meme.data.BaseImage;
import com.atlassian.meme.data.Meme;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DefaultMemeService implements MemeService
{
    private static final AtomicInteger baseImageCounter = new AtomicInteger();
    private static final List<Meme> memes = Lists.newLinkedList();
    private static final List<BaseImage> baseImages = Lists.newLinkedList();

    @Override
    public BaseImage createBaseImage(String contentType, String data)
    {
        int id = baseImageCounter.incrementAndGet();
        String key = "user-submitted-" + id;
        String name = "User submitted";
        return createBaseImage(new BaseImage(contentType, data, key, name));
    }

    @Override
    public BaseImage createBaseImage(BaseImage baseImage)
    {
        baseImages.add(baseImage);
        return baseImage;
    }

    @Override
    public void persist(Meme meme)
    {
        memes.add(meme);
    }

    @Override
    public List<Meme> getMemes()
    {
        return Collections.unmodifiableList(memes);
    }

    @Override
    public Meme getMeme(String key)
    {
        if (null == key)
        {
            throw new IllegalArgumentException("key");
        }
        for (Meme meme : memes)
        {
            if (key.equals(meme.getKey()))
            {
                return meme;
            }
        }
        return null;
    }

    @Override
    public List<BaseImage> getBaseImages()
    {
        return Collections.unmodifiableList(baseImages);
    }

    @Override
    public BaseImage getBaseImage(String key)
    {
        if (null == key)
        {
            throw new IllegalArgumentException("key");
        }
        for (BaseImage baseImage : baseImages)
        {
            if (key.equals(baseImage.getKey()))
            {
                return baseImage;
            }
        }
        return null;
    }

    @Override
    public void clearAll()
    {
        baseImages.clear();
        memes.clear();
        baseImageCounter.set(0);
    }
}
