package com.atlassian.meme.service;

import com.atlassian.meme.data.AddressableBaseImage;
import com.atlassian.meme.data.AddressableMeme;
import com.atlassian.meme.data.BaseImage;
import com.atlassian.meme.data.Meme;

/**
 * Interface for discovering urls
 * @since v1.0
 */
public interface AddressableService
{
    public AddressableMeme meme(String contextPath, Meme meme);

    public Iterable<AddressableMeme> memes(String contextPath, Iterable<Meme> memes);

    public AddressableBaseImage baseImage(String contextPath, BaseImage baseImage);

    public Iterable<AddressableBaseImage> baseImages(String contextPath, Iterable<BaseImage> baseImages);
}
