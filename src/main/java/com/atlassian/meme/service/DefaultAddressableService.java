package com.atlassian.meme.service;

import com.atlassian.meme.data.AddressableBaseImage;
import com.atlassian.meme.data.AddressableMeme;
import com.atlassian.meme.data.BaseImage;
import com.atlassian.meme.data.Meme;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

public class DefaultAddressableService implements AddressableService
{
    @Override
    public AddressableMeme meme(String contextPath, Meme meme)
    {
        return new AddressableMeme(meme.getKey(), meme.getName(),
                contextPath + "/rest/meme/1.0/images/meme/" + meme.getKey());
    }

    @Override
    public Iterable<AddressableMeme> memes(final String contextPath, Iterable<Meme> memes)
    {
        return Iterables.transform(memes, new Function<Meme, AddressableMeme>()
        {
            @Override
            public AddressableMeme apply(Meme meme)
            {
                return meme(contextPath, meme);
            }
        });
    }

    @Override
    public AddressableBaseImage baseImage(String contextPath, BaseImage baseImage)
    {
        return new AddressableBaseImage(baseImage.getKey(), baseImage.getName(),
                contextPath + "/rest/meme/1.0/images/base/" + baseImage.getKey());
    }

    @Override
    public Iterable<AddressableBaseImage> baseImages(final String contextPath, Iterable<BaseImage> baseImages)
    {
        return Iterables.transform(baseImages, new Function<BaseImage, AddressableBaseImage>()
        {
            @Override
            public AddressableBaseImage apply(BaseImage baseImage)
            {
                return baseImage(contextPath, baseImage);
            }
        });
    }
}
