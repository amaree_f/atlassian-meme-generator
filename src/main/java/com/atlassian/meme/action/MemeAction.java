package com.atlassian.meme.action;

import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.meme.data.AddressableBaseImage;
import com.atlassian.meme.data.AddressableMeme;
import com.atlassian.meme.service.AddressableService;
import com.atlassian.meme.service.MemeService;
import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class MemeAction extends JiraWebActionSupport
{
    private final MemeService memeService;
    private final AddressableService addressableService;

    private String upload;
    private String create;
    private String createBaseImageKey;
    private String key;

    public MemeAction(MemeService memeService, AddressableService addressableService)
    {
        this.memeService = memeService;
        this.addressableService = addressableService;
    }

    public void setCreateBaseImageKey(String createBaseImageKey)
    {
        this.createBaseImageKey = createBaseImageKey;
    }

    public void setUpload(String upload)
    {
        this.upload = upload;
    }

    public void setCreate(String create)
    {
        this.create = create;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    @Override
    public String doExecute()
    {
        if (null != upload || null != createBaseImageKey)
        {
            return "createForm";
        }
        if (null != create)
        {
            return "createSelect";
        }
        if (null == key)
        {
            return "gallery";
        }
        return "hero";
    }

    @ActionViewData("hero")
    public Map<String, AddressableMeme> getHeroData()
    {
        return ImmutableMap.of(
                "meme", addressableService.meme(getHttpRequest().getContextPath(), memeService.getMeme(key))
        );
    }

    @ActionViewData("gallery")
    public Map<String, Iterable<AddressableMeme>> getGalleryData()
    {
        return ImmutableMap.of(
                "memes", addressableService.memes(getHttpRequest().getContextPath(), memeService.getMemes())
        );
    }

    @ActionViewData("createSelect")
    public Map<String, Object> getCreateSelectData()
    {
        Iterable<AddressableBaseImage> baseImages = addressableService.baseImages(getHttpRequest().getContextPath(), memeService.getBaseImages());
        String baseImagesJson = new DefaultJaxbJsonMarshaller().marshal(baseImages);
        return ImmutableMap.of(
                "baseImages", baseImages,
                "baseImagesJson", baseImagesJson
        );
    }

    @ActionViewData("createForm")
    public Map<String, Object> getCreateFormData()
    {
        if (null == createBaseImageKey)
        {
            return ImmutableMap.<String, Object>of(
                    "baseImageName", "",
                    "baseImageJson", ""
            );
        }
        else
        {
            AddressableBaseImage baseImage = addressableService.baseImage(getHttpRequest().getContextPath(), memeService.getBaseImage(createBaseImageKey));
            String baseImageJson = new DefaultJaxbJsonMarshaller().marshal(baseImage);
            return ImmutableMap.<String, Object>of(
                    "baseImageName", baseImage.getName(),
                    "baseImageJson", baseImageJson
            );
        }
    }
}
