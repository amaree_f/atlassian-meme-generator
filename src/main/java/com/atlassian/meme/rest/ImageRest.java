package com.atlassian.meme.rest;

import com.atlassian.meme.data.BaseImage;
import com.atlassian.meme.data.Meme;
import com.atlassian.meme.service.MemeService;
import org.apache.commons.codec.binary.Base64;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;


/**
 * Path: contextPath + /rest/meme/1.0/
 */
@Path("/images")
@Consumes({MediaType.APPLICATION_JSON})
public class ImageRest
{
    private final MemeService memeService;

    public ImageRest(MemeService memeService)
    {
        this.memeService = memeService;
    }

    @GET
    @Path("/base/{key}")
    public Response getBase(@PathParam("key") String key)
    {
        BaseImage baseImage = memeService.getBaseImage(key);
        if (null != baseImage)
        {
            return streamImage(baseImage.getContentType(), baseImage.getBase64Data());
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Path("/meme/{key}")
    public Response getMeme(@PathParam("key") String key)
    {
        Meme meme = memeService.getMeme(key);
        if (null != meme)
        {
            return streamImage(meme.getContentType(), meme.getBase64Data());
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    private Response streamImage(String contentType, String base64Data)
    {
        Base64 decoder = new Base64();
        final byte[] imgBytes = decoder.decode(base64Data);

        StreamingOutput stream = new StreamingOutput()
        {
            @Override
            public void write(OutputStream os) throws IOException
            {
                os.write(imgBytes);
                os.flush();
            }
        };
        return Response.ok(stream).header("content-type", contentType).build();
    }
}
