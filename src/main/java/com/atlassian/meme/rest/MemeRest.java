package com.atlassian.meme.rest;

import com.atlassian.meme.data.AddressableMeme;
import com.atlassian.meme.data.Meme;
import com.atlassian.meme.service.AddressableService;
import com.atlassian.meme.service.MemeService;
import com.atlassian.meme.util.HashBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


/**
 * Path: contextPath + /rest/meme/1.0/
 */
@Path("/memes")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class MemeRest
{
    private final MemeService memeService;
    private final AddressableService addressableService;

    public MemeRest(MemeService memeService, AddressableService addressableService)
    {
        this.memeService = memeService;
        this.addressableService = addressableService;
    }

    @GET
    public Response getAll(@QueryParam("start") Integer start, @QueryParam("max") Integer max, @Context HttpServletRequest request)
    {
        if (null == start || start < 0) {
            start = 0;
        }
        if (null == max) {
            max = 10;
        }
        List<Meme> memes = memeService.getMemes();
        int end = Math.min(start + max, memes.size());
        List<Meme> page = memes.subList(start, end);
        Iterable<AddressableMeme> addressablePage = addressableService.memes(request.getContextPath(), page);

        return Response.ok(addressablePage).build();
    }

    @GET
    @Path("{key}")
    public Response get(@PathParam("key") String key, @Context HttpServletRequest request)
    {
        Meme meme = memeService.getMeme(key);
        if (null == meme)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        AddressableMeme addressableMeme = addressableService.meme(request.getContextPath(), meme);

        return Response.ok(addressableMeme).build();
    }

    @POST
    public Response create(MemeDto memeDto, @Context HttpServletRequest request)
    {
        String key = HashBuilder.buildHash(memeDto.contentType, memeDto.base64Data);
        Meme meme = new Meme(key, memeDto.name, memeDto.contentType, memeDto.base64Data);
        memeService.persist(meme);
        AddressableMeme addressableMeme = addressableService.meme(request.getContextPath(), meme);
        return Response.ok(addressableMeme).build();
    }

    @XmlRootElement
    public static class MemeDto
    {
        @XmlElement
        private String contentType;

        @XmlElement
        private String base64Data;

        @XmlElement
        private String name;
    }

    @DELETE
    public Response clearAll()
    {
        memeService.clearAll();
        return Response.ok().build();
    }
}
