package com.atlassian.meme.rest;

import com.atlassian.meme.data.AddressableBaseImage;
import com.atlassian.meme.data.BaseImage;
import com.atlassian.meme.service.AddressableService;
import com.atlassian.meme.service.MemeService;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


/**
 * Path: contextPath + /rest/meme/1.0/
 */
@Path("/baseImages")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class BaseImageRest
{
    private final MemeService memeService;
    private final AddressableService addressableService;

    public BaseImageRest(MemeService memeService, AddressableService addressableService)
    {
        this.memeService = memeService;
        this.addressableService = addressableService;
    }

    @GET
    public Response getAll(@QueryParam("start") Integer start, @QueryParam("max") Integer max, @Context HttpServletRequest request)
    {
        if (null == start || start < 0) {
            start = 0;
        }
        if (null == max) {
            max = 10;
        }
        List<BaseImage> baseImages = memeService.getBaseImages();
        int end = Math.min(start + max, baseImages.size());
        List<BaseImage> page = memeService.getBaseImages().subList(start, end);
        Iterable<AddressableBaseImage> addressablePage = addressableService.baseImages(request.getContextPath(), page);

        return Response.ok(addressablePage).build();
    }

    @GET
    @Path("{key}")
    public Response get(@PathParam("key") String key, @Context HttpServletRequest request)
    {
        BaseImage baseImage = memeService.getBaseImage(key);
        if (null == baseImage)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        AddressableBaseImage addressableBaseImage = addressableService.baseImage(request.getContextPath(), baseImage);
        return Response.ok(addressableBaseImage).build();
    }

    @POST
    @Path("seed")
    public Response seed(final BaseImage baseImage, @Context HttpServletRequest request)
    {
        memeService.createBaseImage(baseImage);
        AddressableBaseImage addressableBaseImage = addressableService.baseImage(request.getContextPath(), baseImage);
        return Response.ok(addressableBaseImage).build();
    }
}
