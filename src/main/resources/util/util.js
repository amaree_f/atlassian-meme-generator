define("util/util", function() {

    var DATA_URI_PATTERN = /^data:(.*);(.*),(.*)$/

    function parseDataUrl(dataUrl) {
        var match = dataUrl.match(DATA_URI_PATTERN);
        if (match.length != 4) {
            console.log("Error parsing data url: '" + dataUrl + "'");
            return null;
        }
        if (match[2] !== "base64") {
            console.log("Data url is not base64 encoded. Found encoding " + match[2] + " for data url: '" + dataUrl + "'");
            return null;
        }
        return {
            contentType: match[1],
            base64Data: match[3]
        };
    }

    return {
        parseDataUrl: parseDataUrl
    };
});