define("util/events", function() {

    function Event() {
        this.subscribers = [];
        // bind on and trigger to this soy that clients can pass around easily
        _.bindAll(this, 'on', 'un', 'trigger');
    }
    Event.prototype.on = function(fn) {
        this.subscribers.push(fn);
    };
    Event.prototype.un = function(fn) {
        this.subscribers.length = 0;
    };
    Event.prototype.trigger = function() {
        var args = Array.prototype.slice.call(arguments);
        var instance = this;
        _.each(this.subscribers, function(subscriber) {
            subscriber.apply(instance, args);
        });
    };

    return {
        create: function() {
            return new Event();
        }
    };
});
