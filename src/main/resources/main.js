define("main", function() {

    var $ = AJS.$;

    var $container;
    var baseUrl = AJS.contextPath() + "/memes";

    var ignoreStateChangeEvent = false;
    var createModuleEventsBound = false;

    function requireCreate() {
        return WRM.require(["wr!com.atlassian.meme.atlassian-meme-generator:create"]).done(function() {
            if (!createModuleEventsBound) {
                createModuleEventsBound = true;
                require("create/main").onCreateFormLoaded(onCreateFormLoaded);
                require("create/main").onMemeCreated(onMemeCreated);
                require("create/main").onSelectReturnToGallery(showGallery);
                require("create/main").onFormReturnToGallery(showGallery);
            }
        });
    }

    function pushUrl(url) {
        ignoreStateChangeEvent = true;
        try {
            History.pushState(null, null, baseUrl + url);
        }
        finally {
            ignoreStateChangeEvent = false;
        }
    }

    History.Adapter.bind(window, 'statechange', function() {
        if (!ignoreStateChangeEvent) {
            console.log("state change");
            handleStateChange();
        }
    });

    function getStateFromUrl() {
        var pathname = window.location.pathname;
        if (pathname.substr(0, baseUrl.length) !== baseUrl) {
            console.log("Not at the correct path");
            return null;
        }
        if (window.location.hash) {
            return window.location.hash;
        }
        else {
            return pathname.slice(baseUrl.length);
        }
    }

    var routes = {
        gallery: /^$/,
        create: /^\/create\/.*$/,
        select: /^\/create$/,
        hero: /^\/(.*)$/
    };

    function handleStateChange() {
        var state = getStateFromUrl();
        if (null === state) {
            return;
        }

        var match;
        if (match = state.match(routes.gallery)) {
            require("gallery/main").load($container);
        }
        else if (match = state.match(routes.create)) {
            requireCreate().done(function() {
                require("create/main").loadCreate($container);
            });
        }
        else if (match = state.match(routes.select)) {
            requireCreate().done(function() {
                require("create/main").loadSelect($container);
            });
        }
        else if (match = state.match(routes.hero)) {
            require("hero/main").load($container, match[1]);
        }
        else {
            console.log("Unrecognised state: " + state);
        }
    }

    function initOnLoad() {
        var state = getStateFromUrl();
        if (null === state) {
            return;
        }

        var match;
        if (match = state.match(routes.gallery)) {
            require("gallery/main").init($container);
        }
        else if (match = state.match(routes.create)) {
            requireCreate().done(function() {
                require("create/main").initCreate($container);
            });
        }
        else if (match = state.match(routes.select)) {
            requireCreate().done(function() {
                require("create/main").initSelect($container);
            });
        }
        else if (match = state.match(routes.hero)) {
            require("hero/main").init($container, match[1]);
        }
        else {
            console.log("Unrecognised state: " + state);
        }
    }

    function showCreateSelect() {
        pushUrl("/create");
        requireCreate().done(function() {
            require("create/main").loadSelect($container);
        });
    }

    function showHeroImage(key) {
        pushUrl("/" + key);
        require("hero/main").load($container, key);
    }

    function onCreateFormLoaded(baseImage) {
        // The Create module handles display internally, so only update the url
        pushUrl("/create/" + (baseImage ? baseImage.key : ""));
    }

    function onMemeCreated(meme) {
        showHeroImage(meme.key);
    }

    function showGallery() {
        pushUrl("");
        require("gallery/main").load($container);
    }

    function init() {
        $container = $("#content");
        initOnLoad();
        require("gallery/main").onSelectCreate(showCreateSelect);
        require("gallery/main").onSelectImage(showHeroImage);
        require("hero/main").onReturnToGallery(showGallery);
    }

    AJS.$(init);

    function loadSampleData() {
        WRM.require("wr!com.atlassian.meme.atlassian-meme-generator:create-base")
            .done(function() {
                require("create-base/main").uploadSampleBaseImages()
                    .done(function() {
                        require("create-base/main").uploadSampleMemes();
                    });
            });
    }

    function clearAll() {
        return $.ajax({
            type: "DELETE",
            url: AJS.contextPath() + "/rest/meme/1.0/memes",
            dataType: "json",
            contentType: "application/json"
        }).done(function() {
                console.log("Cleared all data");
            });
    }

    return {
        loadSampleData: loadSampleData,
        clearAll: clearAll
    }
});
require("main");
