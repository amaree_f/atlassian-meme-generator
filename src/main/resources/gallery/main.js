define("gallery/main", ["../util/events"], function(events) {

    var $ = AJS.$;

    var selectCreate = events.create();
    var selectImage = events.create();

    function initEvents($container) {
        $container.children()
            .delegate(".meme-gallery img", "click", function(e) {
                e.preventDefault();
                var img = $(e.target);
                var key = img.data("key");
                if (key) {
                    selectImage.trigger(img.data("key"));
                }
            });
        $container.find("#create-button").click(function(e) {
            e.preventDefault();
            selectCreate.trigger();
        });
    }

    function loadGallery($container) {
        $.get(AJS.contextPath() + "/rest/meme/1.0/memes")
            .done(function(memes) {
                $container.html(meme.gallery.main({
                    memes: memes
                }));
                initEvents($container);
            });
    }

    function init($container) {
        initEvents($container);
    }

    return {
        init: init,
        load: loadGallery,
        onSelectCreate: selectCreate.on,
        onSelectImage: selectImage.on
    }
});