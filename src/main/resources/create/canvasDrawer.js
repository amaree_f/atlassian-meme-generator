define("create/canvasDrawer", function() {

    var $ = AJS.$;
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");

    var maxCanvasWidth = 640,
        maxCanvasHeight = 480;

    function reset() {
        canvas.width = canvas.width;
    }

    function drawImage(image) {
        reset();
        // Scale to keep aspect ratio
        var xRatio = maxCanvasWidth / image.width;
        var yRatio = maxCanvasHeight / image.height;
        var scale = Math.min(xRatio, yRatio);
        canvas.width = scale * image.width;
        canvas.height = scale * image.height;
        ctx.drawImage(image,
            0, 0, image.width, image.height,
            0, 0, canvas.width, canvas.height);
        console.log("drew image");
    }

    function resetText(size) {
        ctx.lineWidth = 2;
        ctx.strokeStyle = "black";
        ctx.fillStyle = "white";
        ctx.font = "bold " + size + "px Impact";
    }

    function drawCenteredText(text, h, size, reverse) {
        ctx.textAlign = "center";
        var lines = text.split("\n");
        var offset = h;
        if (reverse) {
            offset -= size * (lines.length - 1);
        }
        _.each(lines, function(line) {
            ctx.fillText(line, canvas.width / 2, offset);
            ctx.strokeText(line, canvas.width / 2, offset);
            offset += size;
        });
    }

    function drawTopText(text, size) {
        resetText(size);
        ctx.textBaseline = "top";;
        drawCenteredText(text, 0, size);
    }

    function drawBottomText(text, size) {
        resetText(size);
        ctx.textBaseline = "bottom";
        drawCenteredText(text, canvas.height, size, true);
    }

    function getData() {
        return {
            width: canvas.width,
            height: canvas.height,
            dataUrl: canvas.toDataURL()
        };
    }

    return {
        drawImage: drawImage,
        drawTopText: drawTopText,
        drawBottomText: drawBottomText,
        getData: getData
    };
});