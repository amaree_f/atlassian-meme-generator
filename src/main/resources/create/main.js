define("create/main", ["./fileReader", "./canvasDrawer", "./selectView", "./formView", "../util/util", "../util/events"],
    function(fileReader, canvasDrawer, selectView, formView, util, events) {

    // create functionality

    var $ = AJS.$;

    var createFormLoaded = events.create();
    var memeCreated = events.create();

    var state = {
        $container: null,
        baseImages: null,
        baseImage: null
    };

    formView.onChange(function() {
        draw(state.baseImage);
    });
    formView.onSubmit(function() {
        save(state.baseImage);
    });
    selectView.onSelectImage(function(key) {
        var baseImage = _.find(state.baseImages, function(item) {
            return key === item.key;
        });
        createFormLoaded.trigger(baseImage);
        loadCreate(state.$container, baseImage);
    });
    selectView.onSelectUpload(function(files) {
        fileReader.load(files).done(function(e) {
            var baseImage = util.parseDataUrl(e.target.result);
            baseImage.key = "";
            baseImage.name = "";
            createFormLoaded.trigger(baseImage);
            loadCreate(state.$container, baseImage);
        });
    });

    function drawMeme(data, image) {
        canvasDrawer.drawImage(image);
        console.log("drawing text");
        canvasDrawer.drawTopText(data.topText, data.topSize);
        canvasDrawer.drawBottomText(data.bottomText, data.bottomSize);
        formView.drawImage(canvasDrawer.getData());
    }

    function draw(baseImage) {
        var data = formView.getData();
        var image = new Image();
        image.onload = function() {
            drawMeme(data, image);
        }
        if (baseImage.url) {
            image.src = baseImage.url;
        }
        else {
            image.src = "data:" + baseImage.contentType + ";base64," + baseImage.base64Data;
        }
    }

    function save(baseImage) {
        draw(baseImage);
        var data = util.parseDataUrl(canvasDrawer.getData().dataUrl);
        data.name = formView.getData().name;
        return $.ajax({
            type: "POST",
            url: AJS.contextPath() + "/rest/meme/1.0/memes",
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json"
        }).done(function(meme) {
            console.log("Uploaded meme");
            memeCreated.trigger(meme);
        });
    }

    function loadCreate($container, baseImage) {
        formView.render($container, baseImage && baseImage.name);
        initCreateEvents($container, baseImage);
    }

    function initCreateEvents($container, baseImage) {
        state.baseImage = baseImage;
        draw(baseImage);
    }

    function initCreate($container) {
        formView.bindToRendered($container);
        var baseImage = $container.find(".base-image-json").data("base-image-json");
        initCreateEvents($container, baseImage);
    }

    function loadSelect($container) {
        var baseImages = [];
        var dataPromise = $.get(AJS.contextPath() + "/rest/meme/1.0/baseImages").done(function(_baseImages) {
            _.each(_baseImages, function(baseImage) {
                baseImages.push(baseImage);
            });
        });
        selectView.render($container, dataPromise);
        initSelectEvents($container, baseImages);
    }

    function initSelectEvents($container, baseImages) {
        state.$container = $container;
        state.baseImages = baseImages;
    }

    function initSelect($container) {
        selectView.bindToRendered($container);
        var baseImages = $container.find(".base-images-json").data("base-images-json");
        initSelectEvents($container, baseImages);
    }

    return {
        initSelect: initSelect,
        loadSelect: loadSelect,
        initCreate: initCreate,
        loadCreate: loadCreate,
        onCreateFormLoaded: createFormLoaded.on,
        onMemeCreated: memeCreated.on,
        onSelectReturnToGallery: selectView.onReturnToGallery,
        onFormReturnToGallery: formView.onReturnToGallery
    };
});