define("create/selectView", ["../util/events"], function(events) {

    var $ = AJS.$;

    var $el;

    var onSelectImage = events.create();
    var onSelectUpload = events.create();
    var returnToGallery = events.create();

    function displayBaseImages(baseImages) {
        $el.find(".base-images").html(meme.create.baseImages({
            baseImages: baseImages
        }));
    }

    function render($container, dataPromise) {
        dataPromise.done(displayBaseImages);
        $el = $container.html(meme.create.selectOrUpload({
            baseImages: []
        })).children();
        initEvents();
    }

    function initEvents() {
        $el.find("#create-return-to-gallery").click(function(e) {
            e.preventDefault();
            returnToGallery.trigger();
        });
        $el.delegate(".base-images img", "click", function(e) {
            e.preventDefault();
            var key = $(e.target).data("key");
            console.log("Selected image: " + key);
            onSelectImage.trigger(key);
        });
        $el.find("#meme-create-upload").click(function(e) {
            e.preventDefault();
            var files = $el.find("#meme-create-file")[0].files;
            onSelectUpload.trigger(files);
        });
    }

    function bindToRendered($container) {
        $el = $container.children();
        initEvents();
    }

    function onSelectImage(fn) {
        $el.on("create-select-image", fn);
    }

    function onSelectUpload(fn) {
        $el.on("create-select-upload", fn);
    }

    return {
        bindToRendered: bindToRendered,
        render: render,
        onSelectImage: onSelectImage.on,
        onSelectUpload: onSelectUpload.on,
        onReturnToGallery: returnToGallery.on
    };
});