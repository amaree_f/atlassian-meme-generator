define("create/fileReader", function() {

    // Filter from https://developer.mozilla.org/en-US/docs/Web/API/FileReader
    var fileFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

    function loadImageFile(files) {
        var deferred = $.Deferred();
        var fileReader = new FileReader();
        fileReader.onload = function (e) {
            deferred.resolve(e);
        };
        fileReader.onerror = function(e) {
            deferred.reject(e);
        };
        if (!files.length) {
            deferred.reject("0 files");
        }
        else {
            var file = files[0];
            if (!fileFilter.test(file.type)) {
                deferred.reject("You must select a valid image file!");
            }
            else {
                fileReader.readAsDataURL(file);
            }
        }
        return deferred.promise();
    }

    return {
        load: loadImageFile
    };
});