define("create/formView", ["../util/events"], function(events) {

    var $el;

    var onChange = events.create();
    var onSubmit = events.create();
    var returnToGallery = events.create();

    function setDefaultData() {
        $el.find("#create-form-top-size").val("62");
        $el.find("#create-form-bottom-size").val("38");
    }

    function render($container, baseImageName) {
        $el = $container.html(meme.create.createForm({
            baseImageName: baseImageName
        })).children();
        setDefaultData();
        initEvents();
    }

    function bindToRendered($container) {
        $el = $container.children();
        setDefaultData();
        initEvents();
    }

    function initEvents() {
        $el.find("input, textarea, select").on("change", function() {
            console.log("chnager");
            onChange.trigger();
        });
        $el.find("#create-form").on("submit", function(e) {
            console.log("submit");
            e.preventDefault();
            onSubmit.trigger();
        });
        $el.find("#create-form-cancel").click(function(e) {
            e.preventDefault();
            returnToGallery.trigger();
        });
    }

    function drawImage(data) {
        $("#create-image").width(data.width).height(data.height).attr("src", data.dataUrl);
    }

    function getData() {
        return {
            name: $el.find("#create-form-name").val(),
            topText: $el.find("#create-form-top-text").val(),
            topSize: $el.find("#create-form-top-size").val(),
            bottomText: $el.find("#create-form-bottom-text").val(),
            bottomSize: $el.find("#create-form-bottom-size").val()
        };
    }

    return {
        bindToRendered: bindToRendered,
        render: render,
        drawImage: drawImage,
        getData: getData,
        onChange: onChange.on,
        onSubmit: onSubmit.on,
        onReturnToGallery: returnToGallery.on
    };
});