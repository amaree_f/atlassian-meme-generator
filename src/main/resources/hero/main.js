define("hero/main", ["../util/events"], function(events) {

    var $ = AJS.$;

    var returnToGallery = events.create();

    function initEvents($container) {
        $container.find("#hero-return-to-gallery").click(function(e) {
            e.preventDefault();
            returnToGallery.trigger();
        });
    }

    function init($container, key) {
        initEvents($container);
    }

    function load($container, key) {
        $.get(AJS.contextPath() + "/rest/meme/1.0/memes/" + key).done(function(memeData) {
            $container.html(meme.hero.main({
                meme: memeData
            }));
            initEvents($container);
        }).fail(function() {
            console.log("Meme not found for key: " + key);
        });
    }

    return {
        init: init,
        load: load,
        onReturnToGallery: returnToGallery.on
    };
});