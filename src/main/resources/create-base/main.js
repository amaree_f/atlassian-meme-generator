define("create-base/main", ["../util/util"], function(util) {

    var $ = AJS.$;

    // functionality to upload base images

    function postSeed(key, name, dataUrl) {
        var imageData = util.parseDataUrl(dataUrl);
        if (!imageData) {
            return;
        }
        console.log("Uploading image: " + key);
        var data = {
            key: key,
            name: name,
            contentType: imageData.contentType,
            base64Data: imageData.base64Data
        };
        return $.ajax({
            type: "POST",
            url: AJS.contextPath() + "/rest/meme/1.0/baseImages/seed",
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json"
        }).done(function() {
            console.log("Uploaded image: " + key);
        });
    }

    function renderToCanvas(img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        return canvas.toDataURL();
    }

    function uploadBaseImage(key, name, src) {
        var deferred = $.Deferred();
        console.log("Rendering image: " + key);
        var img = new Image();
        img.onload = function() {
            var dataUrl = renderToCanvas(img);
            postSeed(key, name, dataUrl)
                .done(function(data) {
                    deferred.resolve(data);
                })
                .fail(function() {
                    deferred.reject();
                });
        };
        img.src = src;
        return deferred.promise();
    }

    function uploadSampleBaseImages() {
        var root = AJS.contextPath() + "/download/resources/com.atlassian.meme.atlassian-meme-generator:base-images/base-images/";
        var deferreds = [
            uploadBaseImage("first-world-problems", "First World Problems", root + "first-world-problems.jpg"),
            uploadBaseImage("one-does-not-simply", "One Does Not Simply", root + "one-does-not-simply.jpg"),
            uploadBaseImage("the-most-interesting-man-in-the-world", "The Most Interesting Man In The World", root + "the-most-interesting-man-in-the-world.jpg"),
            uploadBaseImage("y-u-no", "Y U NO", root + "y-u-no.jpg"),
            uploadBaseImage("success-kid", "Success Kid", root + "success-kid.jpg"),
            uploadBaseImage("xzibit-yo-dawg", "Yo Dawg", root + "xzibit-yo-dawg.jpg")
        ];
        return $.when.apply($, deferreds);
    }

    function uploadMeme(baseImageKey, name, topText, topTextSize, bottomText, bottomTextSize) {
        var canvasDrawer = require("create/canvasDrawer");
        $.get(AJS.contextPath() + "/rest/meme/1.0/baseImages/" + baseImageKey)
            .done(function(baseImageData) {
                var image = new Image();
                image.onload = function() {
                    canvasDrawer.drawImage(image);
                    canvasDrawer.drawTopText(topText, topTextSize);
                    canvasDrawer.drawBottomText(bottomText, bottomTextSize);
                    var data = util.parseDataUrl(canvasDrawer.getData().dataUrl);
                    data.name = name;
                    $.ajax({
                        type: "POST",
                        url: AJS.contextPath() + "/rest/meme/1.0/memes",
                        data: JSON.stringify(data),
                        dataType: "json",
                        contentType: "application/json"
                    }).done(function(meme) {
                            console.log("Uploaded meme " + name);
                        });
                };
                image.src = baseImageData.url;
            });
    }

    function uploadSampleMemes() {
        return WRM.require(["wr!com.atlassian.meme.atlassian-meme-generator:create"]).done(function() {
            uploadMeme("xzibit-yo-dawg", "Yo Dawg", "Yo Dawg", 62, "I heard you like urls in your urls\nso I put /jira/secure/MemeAction.jspa\nin your /jira/plugins/servlet/memes", 38)
            uploadMeme("the-most-interesting-man-in-the-world", "I Don't Always", "I DON'T ALWAYS\nWRITE HTML", 38, "BUT WHEN I DO, I RELOAD\nTHE ENTIRE PAGE", 38);
            uploadMeme("y-u-no", "Y U NO", "Y U NO", 62, "WRITE 'HTML' IN 'HTML' PAGE", 38);
            uploadMeme("one-does-not-simply", "One Does Not Simply", "ONE DOES NOT SIMPLY", 62, "WRITE JSON INTO A PAGE\nWITHOUT CREATING AN XSS HOLE", 38);
            uploadMeme("success-kid", "Success Kid", "GOF DESIGN PATTERNS", 62, "IN JAVASCRIPT", 84);
            uploadMeme("first-world-problems", "First World Problems", "THIS WEB PAGE", 62, "TAKES 200 MS TOO LONG TO LOAD", 38);
        });
    }

    return {
        uploadSampleBaseImages: uploadSampleBaseImages,
        uploadSampleMemes: uploadSampleMemes
    };
});