
To run:
    atlas-run --product jira
or
    atlas-debug --product jira
then
    atlas-cli --context-path /jira -p 2990

To upload sample data:
    require("main").loadSampleData();
    require("main").loadSampleMemes();
To clear all data:
    require("main").clearAll();

Definitely doesn't work in IE8 because it uses canvas. IE9 is untested.
